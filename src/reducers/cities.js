import * as constants from '../constants';

const INITIAL_STATE = { cities: [] };

export default function(state = INITIAL_STATE, action){
    switch (action.type) {
      case constants.CITIES:
          return {...state, cities: action.payload.data };
      default:
        return state;
    }
}
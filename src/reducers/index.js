import { combineReducers } from 'redux';
import cities from './cities';
const rootReducer = combineReducers({
    state: (state = {}) => state,
    cities
});

export default rootReducer;
import axios from 'axios';
import * as constants from '../constants';

export function getCities(){
    const request = axios.get(`${constants.API_URL}`);
    return {
      type: constants.CITIES,
      payload: request
    }
}
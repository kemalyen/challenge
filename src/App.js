import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getCities } from './action';
import logo from './logo.svg';
import './App.css';


class App extends Component {

  componentWillMount(){
    this.props.dispatch(getCities());
  }

  listSubs(id){
    return (
      this.props.cities.map((city, index) => {
        if (city.hasOwnProperty('parentId') &&  city.parentId === id) {
          return (
            <div key={index}> <i>{index} -  {city.name}</i></div>
          ) 
        }          
      }
      
      ));
  }

  listCities(){
    return (
      this.props.cities.map((city, index) => {
        if (!city.hasOwnProperty('parentId')) {
          return (
            <div key={index}><strong>{index} -  {city.name} </strong> <br/>
              <div>{this.listSubs(city.id)}</div>
            </div>
          ) 
        }          
      }

      ));
  }

  render() {
    if(!this.props.cities){
      return (<div>Loading ...</div>)
   }
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>

        <h3>Cities will come here</h3>
        {this.listCities()}
      </div>
    );
  }
}


function mapStateToProps(state) {
  const { cities } = state.cities;
  return { cities };
}

export default connect(mapStateToProps)(App);
